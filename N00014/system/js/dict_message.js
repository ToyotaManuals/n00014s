/*! All Rights Reserved. Copyright 2012 (C) TOYOTA  MOTOR  CORPORATION.
Last Update: 2012/10/22 */

/**
 * file dict.js<br />
 *
 * @fileoverview 多言語対応辞書クラス。（西語）<br />
 * file-> dict_message.js
 * @author Imamura
 * @version 1.0.0
 *
 * History(date|version|name|desc)<br />
 *  2012/10/22|1.0.0|Imamura|新規作成<br />
 */
/*-----------------------------------------------------------------------------
 * サービス情報高度化プロジェクト
 * 更新履歴
 * 2012/10/22 Imamura ・新規作成
 *---------------------------------------------------------------------------*/
/**
 * 多言語対応辞書クラス（西語）
 * @namespace 多言語対応辞書クラス（西語）
 */
var Dict = {
  //key: value,     Translate the value of dialogue box message or title.  Don't change key.
  MVWF0011AAE:                        'Falló el inicio de Techstream.\nConfirme que se cumplen las siguientes condiciones:\n- Techstream está instalado.\n- Techstream puede iniciarse independientemente.\nCódigo de error: ',
  MVWF0012AAE:                        'La operación falló porque Techstream estaba procesando.\nReintentar tras confirmar la información de \nTechstream que aparece en pantalla.\nCódigo de error: ',
  MVWF0013AAE:                        'La operación falló porque la siguiente función estaba ejecutándose.\nFinalizar la siguiente función y reintentar.\n  Utilidad, CARB OBD II, CUW, SUW\nCódigo de error: ',
  MVWF0014AAE:                        'La operación falló.\nSi Techstream ya está iniciado, finalizar Techstream y reintentar.\nCódigo de error: ',
  MVWF0015AAE:                        'La operación falló.\nCódigo de error:',
  MVWF0016AAE:                        'La operación falló.\nConfirme que se cumple la siguiente condición:\n- Techstream está instalado.\nCódigo de error: ',
  MVWF0017AAE:                        'La operación falló porque Techstream está ejecutándose \nen otro navegador.\nCerrar el otro navegador y reintentar.\nCódigo de error: ',
  MVWF0021AAI:                        'Preparando...',
  MVWF0022AAI:                        'Preparación: OK',
  MVWF0023AAI:                        'Iniciando Techstream...',
  MVWF0024AAI:                        'Inicio Techstream: OK',
  MVWF0025AAI:                        'Iniciando función...',
  MVWF0123DAE:                        'Ocurrió un error inesperado.',
  MVWF0124DAE:                        'Cerrar el navegador y reintentar.',
  MVWF0125DAE:                        'Error del sistema',
  CONST_FACEBOX_IMAGE_TITLE:          'Salir',
  CONST_GTS_FACEBOX_TITLE:            'Enviando/recibiendo GTS...',
  CONST_CONTENTS_TITLE_PRINT:         'Imprimir - Toyota Service Information',
  CONST_CONTENTS_TITLE_DIAGNOSTIC:    'Ayuda de diagnóstico - Toyota Service Information',
  CONST_CONTENTS_TITLE_INSPECTION:    'Procedimiento de inspección - Toyota Service Information',
  CONST_CONTENTS_TITLE_REFERENCE:     'Página de referencia - Toyota Service Information',
  CONST_CONTENTS_FIXED_HEAD:          '<div class="fontEs" id="header_head"><div id="head_buttons"><input type="button" class="button_style" id="btn_print" value="Imprimir"></div><div id="navigation"><p><a href="javascript:void(0);" class="link" id="lnk_close">Salir</a></p></div></div>',
  CONST_CONTENTS_FIXED_HEAD_IE:       '<div class="fontEs" id="header_head"><div id="head_buttons"><input type="button" class="button_style" id="btn_print" value="Imprimir"><input type="button" class="button_style" id="btn_printpreview" value="Vista preliminar"></div><div id="navigation"><p><a href="javascript:void(0);" class="link" id="lnk_close">Salir</a></p></div></div>',
  CONST_CONTENTS_FIXED_FOOT:          '<div class="fontEs" id="footer_message">&copy; 2012 TOYOTA MOTOR CORPORATION. All Rights Reserved.</div>',
  CONST_CONTENTS_FLOW_NAVE_TITLE:   'Visión general',
  CONST_CONTENTS_FLOW_BTN_PROC:     'Procedimientos',
  CONST_CONTENTS_FLOW_BTN_JUDGE:    'Criterios'
};

// START_MARK: Don't change below program code between START_MARK and END_MARK.
/**
 * 初期化
 */
Dict.$init = function() {
  var METHODNAME = 'Dict.$init';
  try {

    var len = DictConst.Keys.length;
    //DictConstの定義を言語ごとの定義にセット
    for(var i = 0; i < len; i++) {
      var key = DictConst.Keys[i];
      Dict[key] = DictConst[key];
    }

  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};
// END_MARK: Don't change above program code between START_MARK and END_MARK.
